import TimeNow from "./components/timeNow";

function App() {
  return (
    <div>
      <TimeNow/>
    </div>
  );
}

export default App;
