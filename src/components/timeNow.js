import { Component } from "react";
let today = new Date();
let hours = today.getHours();
let ampm = hours >= 12 ? "pm" : "am ";
let minute = today.getMinutes();
let second = today.getSeconds();

class TimeNow extends Component{
    constructor(props){
        super(props)

        this.state = {
            hours: hours,
            minute: minute,
            second: second,
            ampm: ampm
        }
    }
    clickHandlerEvent = () => {
        let today = new Date();
        this.setState({
            hours: today.getHours(),
            minute: today.getMinutes(),
            second: today.getSeconds(),
            ampm: hours >= 12 ? "pm" : "am "
        })
    }
    render(){
        return(
            <div>
                <button onClick={this.clickHandlerEvent}>Change Color</button>
                <h1>Hello World</h1>
                {this.state.second % 2 === 0 ? <p style={{color: "red"}}>{this.state.hours}:{this.state.minute}:{this.state.second} {this.state.ampm}</p>:
                <p style={{color: "blue"}}>{this.state.hours}:{this.state.minute}:{this.state.second} {this.state.ampm}</p>
                }
                
            </div>
        )
    }
}
export default TimeNow;